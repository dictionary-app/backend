import { GroupDictionaryModel } from './domains/groups/group-dictionary.model'
import { GroupModel } from './domains/groups/group.model'
import { DictionaryModel } from './domains/dictionary/dictionary.model'
import { UserModel } from './domains/users/user.model'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { SequelizeModule } from '@nestjs/sequelize'
import { AuthModule } from './domains/auth/auth.module'
import { UsersModule } from './domains/users/users.module'
import { DictionaryModule } from './domains/dictionary/dictionary.module'
import { GroupsModule } from './domains/groups/groups.module'
import { DictionaryUserModel } from './domains/dictionary/dictionary-users.model'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.${process.env.NODE_ENV}.env`,
    }),
    // SequelizeModule.forRoot({
    //   dialect: 'postgres',
    //   host: process.env.POSTGRES_HOST,
    //   port: Number(process.env.POSTGRES_PORT),
    //   username: process.env.POSTGRES_USER,
    //   password: process.env.POSTGRES_PASSWORD,
    //   database: process.env.POSTGRES_DB,
    //   models: [UserModel, DictionaryModel, DictionaryUserModel, GroupDictionaryModel, GroupModel],
    //   autoLoadModels: true,
    //   sync: { force: false },
    // }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'english',
      password: '1234',
      database: 'english',
      models: [UserModel, DictionaryModel, DictionaryUserModel, GroupDictionaryModel, GroupModel],
      autoLoadModels: true,
      sync: { force: false },
    }),
    AuthModule,
    UsersModule,
    DictionaryModule,
    GroupsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
