export const SEARCH_LINGUALEO_DICTIONARY_API = 'https://api.lingualeo.com/getTranslates'

export const GET_LINGUALEO_DICTIONARY_API = 'https://api.lingualeo.com/GetWords'

export const SET_LINGUALEO_DICTIONARY_API = 'https://api.lingualeo.com/SetWords'

export const LOGIN_LEO_API = 'https://lingualeo.com/api/auth'
