import { LEO_TOKEN } from './../configs/tokens'
import { Request } from 'express'

export const getLeoToken = (request: Request) => {
  let token = null
  if (request && request.cookies) token = request.cookies[LEO_TOKEN]
  return token
}
