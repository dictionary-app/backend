import { GroupModel } from './../groups/group.model'
import { DictionaryModel } from './../dictionary/dictionary.model'
import { forwardRef, Module } from '@nestjs/common'
import { SequelizeModule } from '@nestjs/sequelize'
import { AuthModule } from 'src/domains/auth/auth.module'
import { UserModel } from './user.model'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'
import { DictionaryUserModel } from '../dictionary/dictionary-users.model'

@Module({
  controllers: [UsersController],
  providers: [UsersService],
  imports: [
    SequelizeModule.forFeature([UserModel, DictionaryModel, DictionaryUserModel, GroupModel]),
    forwardRef(() => AuthModule),
  ],
  exports: [UsersService],
})
export class UsersModule {}
