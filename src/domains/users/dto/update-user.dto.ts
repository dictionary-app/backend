import { PartialType } from '@nestjs/swagger'
import { UserEntity } from '../entity/user.entity'

export class UpdateUserDto extends PartialType(UserEntity) {
  email: string
  name: string
}
