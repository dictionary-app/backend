import { GroupEntity } from './../groups/entity/group.entity'
import { GroupModel } from './../groups/group.model'
import { DictionaryEntity } from './../dictionary/entity/dictionary.entity'
import { DictionaryModel } from './../dictionary/dictionary.model'
import { ApiProperty } from '@nestjs/swagger'
import { BelongsToMany, Column, DataType, HasMany, Model, Table } from 'sequelize-typescript'
import { DictionaryUserModel } from '../dictionary/dictionary-users.model'
import { IUser } from './types'

@Table({ tableName: 'users' })
export class UserModel extends Model<UserModel, IUser> {
  @ApiProperty({ description: 'Уникальный идентификатор' })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number

  @ApiProperty({ example: 'Иван', description: 'Имя пользователя' })
  @Column({ type: DataType.STRING, allowNull: false })
  name: string

  @ApiProperty({ example: 'email@mail.ru', description: 'E-mail пользователя' })
  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  email: string

  @ApiProperty({ example: 'password1234', description: 'Пароль пользователя' })
  @Column({ type: DataType.STRING, allowNull: true })
  password: string

  @BelongsToMany(() => DictionaryModel, () => DictionaryUserModel)
  dictionary: DictionaryEntity[]

  @HasMany(() => GroupModel)
  groups: GroupEntity[]
}
