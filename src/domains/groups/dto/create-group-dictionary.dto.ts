import { ApiProperty } from '@nestjs/swagger'
export class CreateGroupDictionaryDto {
  @ApiProperty({ example: '1', description: 'Идентификатор слова' })
  dictionaryId: number

  @ApiProperty({ example: '1', description: 'Идентификатор набора' })
  groupId: number
}
