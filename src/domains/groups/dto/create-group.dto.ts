import { IGroupsItem } from './../types/index'
import { ApiProperty } from '@nestjs/swagger'
export class CreateGroupDto implements IGroupsItem {
  @ApiProperty({ example: 'Набор для слов', description: 'Название набора' })
  title: string
}
