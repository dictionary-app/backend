import { ApiProperty } from '@nestjs/swagger'
export class RemoveGroupDictionaryDto {
  @ApiProperty({ example: '1', description: 'Идентификатор слова' })
  dictionaryId: number

  @ApiProperty({ example: '1', description: 'Идентификатор набора' })
  groupId: number
}
