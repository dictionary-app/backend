import { CreateGroupDictionaryDto } from './dto/create-group-dictionary.dto'
import { ApiProperty } from '@nestjs/swagger'
import { Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript'
import { DictionaryModel } from '../dictionary/dictionary.model'
import { GroupModel } from '../groups/group.model'

@Table({ tableName: 'dictionary-groups', createdAt: false, updatedAt: false })
export class GroupDictionaryModel extends Model<GroupDictionaryModel, CreateGroupDictionaryDto> {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number

  @ForeignKey(() => DictionaryModel)
  @ApiProperty({ example: '1' })
  @Column({ type: DataType.INTEGER, allowNull: false })
  dictionaryId: number

  @ForeignKey(() => GroupModel)
  @ApiProperty({ example: '3' })
  @Column({ type: DataType.INTEGER, allowNull: false })
  groupId: number
}
