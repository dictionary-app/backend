import { DictionaryModel } from './../dictionary/dictionary.model'
import { UsersService } from './../users/users.service'
import { GroupModel } from './group.model'
import { HttpModule, Module } from '@nestjs/common'
import { GroupsService } from './groups.service'
import { GroupsController } from './groups.controller'
import { SequelizeModule } from '@nestjs/sequelize'
import { UserModel } from '../users/user.model'
import { GroupDictionaryModel } from './group-dictionary.model'

@Module({
  controllers: [GroupsController],
  imports: [HttpModule, SequelizeModule.forFeature([GroupModel, UserModel, GroupDictionaryModel, DictionaryModel])],
  providers: [GroupsService, UsersService],
})
export class GroupsModule {}
