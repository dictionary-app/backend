import { GroupDictionaryModel } from './group-dictionary.model'
import { UserEntity } from './../users/entity/user.entity'
import { IGroupsItem } from './types/index'
import { ApiProperty } from '@nestjs/swagger'
import { Column, DataType, Model, Table, BelongsTo, ForeignKey, BelongsToMany } from 'sequelize-typescript'
import { UserModel } from '../users/user.model'
import { DictionaryEntity } from '../dictionary/entity/dictionary.entity'
import { DictionaryModel } from '../dictionary/dictionary.model'

@Table({ tableName: 'groups' })
export class GroupModel extends Model<GroupModel, Omit<IGroupsItem, 'id'>> {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number

  @ForeignKey(() => UserModel)
  @ApiProperty({ example: 1, description: 'Идентификатор юзера' })
  @Column({ type: DataType.INTEGER })
  userId: number

  @ApiProperty({ example: 'Набор для слов', description: 'Название набора' })
  @Column({ type: DataType.STRING, allowNull: true })
  title: string

  @BelongsTo(() => UserModel)
  user: UserEntity

  @BelongsToMany(() => DictionaryModel, () => GroupDictionaryModel)
  dictionary: DictionaryEntity[]
}
