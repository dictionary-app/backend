import { IGroupsItem } from './../types/index'
import { ApiProperty } from '@nestjs/swagger'

export class GroupEntity implements IGroupsItem {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  id: number

  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  userId: number

  @ApiProperty({ example: 'Набор для слов', description: 'Название набора' })
  title: string
}
