import { DictionaryModel } from './../dictionary/dictionary.model'
import { GroupEntity } from './entity/group.entity'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { getBadRequest } from 'src/helpers'
import { UsersService } from '../users/users.service'
import { CreateGroupDto } from './dto/create-group.dto'
import { UpdateGroupDto } from './dto/update-group.dto'
import { GroupModel } from './group.model'
import { CreateGroupDictionaryDto } from './dto/create-group-dictionary.dto'
import { RemoveGroupDictionaryDto } from './dto/remove-group-dictionary.dto'
import { GroupDictionaryModel } from './group-dictionary.model'

@Injectable()
export class GroupsService {
  constructor(
    private userService: UsersService,
    @InjectModel(DictionaryModel) private dictionaryRepository: typeof DictionaryModel,
    @InjectModel(GroupModel) private groupRepository: typeof GroupModel,
    @InjectModel(GroupDictionaryModel) private groupDictionaryRepository: typeof GroupDictionaryModel
  ) {}
  async create(createGroupDto: CreateGroupDto, userToken: string) {
    try {
      const { id: userId } = await this.userService.getUserByToken(userToken)
      return this.groupRepository.create({ ...createGroupDto, userId })
    } catch (e) {
      getBadRequest('Не получилось создать набор')
    }
  }

  async findAll(userToken: string): Promise<GroupEntity[]> {
    const { id } = await this.userService.getUserByToken(userToken)
    return this.groupRepository.findAll({
      include: {
        attributes: [],
        where: { id },
        required: true,
        association: 'user',
      },
    })
  }

  findOne(id: number) {
    try {
      return this.groupRepository.findOne({
        where: { id },
      })
    } catch (e) {
      getBadRequest('Не удалось получить набор')
    }
  }

  async update(id: number, userToken: string, updateGroupDto: Omit<UpdateGroupDto, 'userId'>) {
    const { id: userId } = await this.userService.getUserByToken(userToken)
    return this.groupRepository.update({ ...updateGroupDto }, { where: { id, userId } })
  }

  async remove(id: number) {
    try {
      await this.removeAllWordsFromGroup(id)
      return this.groupRepository.destroy({
        where: { id },
      })
    } catch (e) {
      getBadRequest('Не получилось удалить набор')
    }
  }

  /**
   *  Получение слов
   * @param userToken string
   * @returns Promise<DictionaryEntity[]>
   */
  async findAllWordsByGroup(groupId: number) {
    try {
      return this.dictionaryRepository.findAll({
        include: {
          attributes: [],
          where: {
            id: groupId,
          },
          required: true,
          association: 'groups',
        },
      })
    } catch (e) {
      getBadRequest('Не получилось получить список слов набора')
    }
  }

  /**
   * Добавление слова в набор
   * @param word CreateGroupDictionaryDto
   * @param request Request
   * @returns
   */
  async addWordToGroup(item: CreateGroupDictionaryDto) {
    try {
      return this.groupDictionaryRepository.findOrCreate({
        raw: true,
        where: item,
        defaults: item,
      })
    } catch (e) {
      getBadRequest('Не удалось добавить слово в набор')
    }
  }

  /**
   * Удаление слова из набора
   * @param word RemoveDictionaryRequestDto
   * @param request Request
   * @returns
   */
  removeWordFromGroup(item: RemoveGroupDictionaryDto) {
    try {
      return this.groupDictionaryRepository.destroy({ where: item })
    } catch (e) {
      getBadRequest('Не получилось удалить слово из набора')
    }
  }

  /**
   * Удаление всех слов из набора
   * @param word RemoveDictionaryRequestDto
   * @param request Request
   * @returns
   */
  removeAllWordsFromGroup(groupId: number) {
    try {
      return this.groupDictionaryRepository.destroy({
        where: {
          groupId,
        },
      })
    } catch (e) {
      getBadRequest('Не удалось удалить слово из набора')
    }
  }
}
