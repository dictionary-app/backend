import { Controller, Get, Post, Body, Patch, Param, Delete, Req, UseGuards } from '@nestjs/common'
import { GroupsService } from './groups.service'
import { CreateGroupDto } from './dto/create-group.dto'
import { UpdateGroupDto } from './dto/update-group.dto'
import { ApiCookieAuth, ApiTags } from '@nestjs/swagger'
import { Request } from 'express'
import { getLocalToken } from 'src/helpers'
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard'
import { CreateGroupDictionaryDto } from './dto/create-group-dictionary.dto'
import { RemoveGroupDictionaryDto } from './dto/remove-group-dictionary.dto'

@Controller('groups')
@ApiTags('Groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Post()
  create(@Body() createGroupDto: CreateGroupDto, @Req() request: Request) {
    return this.groupsService.create(createGroupDto, getLocalToken(request))
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Get()
  findAll(@Req() request: Request) {
    return this.groupsService.findAll(getLocalToken(request))
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.groupsService.findOne(id)
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Patch(':id')
  update(@Param('id') id: number, @Body() updateGroupDto: UpdateGroupDto, @Req() request: Request) {
    return this.groupsService.update(id, getLocalToken(request), updateGroupDto)
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Get('dictionary/:id')
  findAllWordsByGroup(@Param('id') id: number) {
    return this.groupsService.findAllWordsByGroup(id)
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Post('dictionary')
  addWordToGroup(@Body() item: CreateGroupDictionaryDto) {
    return this.groupsService.addWordToGroup(item)
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Delete('dictionary')
  removeWordFromGroup(@Body() item: RemoveGroupDictionaryDto) {
    return this.groupsService.removeWordFromGroup(item)
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.groupsService.remove(id)
  }
}
