import { GroupDictionaryModel } from './../groups/group-dictionary.model'
import { GroupModel } from './../groups/group.model'
import { GroupEntity } from './../groups/entity/group.entity'
import { UserModel } from './../users/user.model'
import { ApiProperty } from '@nestjs/swagger'
import { BelongsToMany, Column, DataType, Model, Table } from 'sequelize-typescript'
import { UserEntity } from '../users/entity/user.entity'
import { DictionaryUserModel } from './dictionary-users.model'
import { DictionaryItemDto } from './dto/dictionary-item.dto'

@Table({ tableName: 'dictionary' })
export class DictionaryModel extends Model<DictionaryModel, Omit<DictionaryItemDto, 'localId'>> {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  localId: number

  @ApiProperty({ example: '21312312312', description: 'Идентификатор слова' })
  @Column({ type: DataType.INTEGER, allowNull: true })
  id: number

  @ApiProperty({ example: '123123123213', description: 'Идентификатор перевода слова' })
  @Column({ type: DataType.INTEGER, allowNull: true })
  twordId: number

  @ApiProperty({ example: 'lingua', description: 'Английское значение слова' })
  @Column({ type: DataType.STRING, allowNull: true })
  wordValue: string

  @ApiProperty({ example: 'lˈɪŋgwə', description: 'Транскрипция' })
  @Column({ type: DataType.STRING, allowNull: true })
  transcription: string

  @ApiProperty({ example: 'язык,языковой', description: 'Перевод' })
  @Column({ type: DataType.STRING, allowNull: true })
  translateValue: string

  @ApiProperty({ example: 'https://google.com/image.png', description: 'Url картинки' })
  @Column({ type: DataType.STRING, allowNull: true })
  picture: string

  @ApiProperty({ example: 'We will not permit this, Alfonso and I!', description: 'Пример использования' })
  @Column({ type: DataType.STRING, allowNull: true })
  useCase: string

  @ApiProperty({ example: 'https:///bGluZ3Vhc7c9e6d1.mp3', description: 'Url озвучки' })
  @Column({ type: DataType.STRING, allowNull: true })
  pronunciation: string

  @BelongsToMany(() => UserModel, () => DictionaryUserModel)
  users: UserEntity[]

  @BelongsToMany(() => GroupModel, () => GroupDictionaryModel)
  groups: GroupEntity[]
}
