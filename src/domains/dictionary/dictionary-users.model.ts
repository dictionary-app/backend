import { UserModel } from './../users/user.model'
import { DictionaryModel } from './dictionary.model'
import { ApiProperty } from '@nestjs/swagger'
import { Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript'
import { IDictionaryUserItem } from './types'

@Table({ tableName: 'dictionary-users', createdAt: false, updatedAt: false })
export class DictionaryUserModel extends Model<DictionaryUserModel, Omit<IDictionaryUserItem, 'id'>> {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number

  @ForeignKey(() => DictionaryModel)
  @ApiProperty({ example: '1' })
  @Column({ type: DataType.INTEGER, allowNull: false })
  dictionaryId: number

  @ForeignKey(() => UserModel)
  @ApiProperty({ example: '3' })
  @Column({ type: DataType.INTEGER, allowNull: false })
  userId: number
}
