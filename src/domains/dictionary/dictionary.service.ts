import { IDictionaryItem, RESPONSE_STATUSES } from 'src/domains/dictionary/types'
import { getLocalToken } from './../../helpers/getLocalToken'
import { getLeoToken } from './../../helpers/getLeoToken'
import { AxiosResponse } from 'axios'
import { Observable } from 'rxjs'
import { Request } from 'express'
import { HttpService, Injectable } from '@nestjs/common'
import { SEARCH_LINGUALEO_DICTIONARY_API, SET_LINGUALEO_DICTIONARY_API } from 'src/configs'
import { InjectModel } from '@nestjs/sequelize'
import { DictionaryModel } from './dictionary.model'
import { DictionaryUserModel } from './dictionary-users.model'
import { UsersService } from '../users/users.service'
import { getBadRequest } from 'src/helpers'
import { DictionaryItemDto } from './dto/dictionary-item.dto'
import { RemoveDictionaryRequestDto } from './dto/remove-dictionary-item.dto'
import {
  formatRequestSearchLeo,
  addLeoDictionary,
  getLeoDictionary,
  getLeoRequest,
  getRemoveData,
} from './dictionary.helpers'
import { IUser } from '../users/types'

@Injectable()
export class DictionaryService {
  constructor(
    private readonly httpService: HttpService,
    private userService: UsersService,
    @InjectModel(DictionaryModel) private dictionaryRepository: typeof DictionaryModel,
    @InjectModel(DictionaryUserModel) private dictionaryUserRepository: typeof DictionaryUserModel
  ) {}

  /**
   * Поиск слова в словаре Лео и вывод в правильной структуре
   * @param text string
   * @param token string
   * @returns Promise<Omit<IDictionaryItem, 'localId'>[]>
   */
  async search(text: string, token: string): Promise<Omit<IDictionaryItem, 'localId'>[]> {
    return formatRequestSearchLeo(await getLeoRequest(token, SEARCH_LINGUALEO_DICTIONARY_API, { text }))
  }

  /**
   * Поиск слова в словаре Лео и вывод в структуре Лео
   * @param text string
   * @param token string
   * @returns any
   */
  originSearch(text: string, token: string): Promise<Observable<AxiosResponse>> {
    return getLeoRequest(token, SEARCH_LINGUALEO_DICTIONARY_API, { text })
  }

  /**
   * Получение массива добавленных слов пользователя
   * @param userToken string
   * @returns Promise<DictionaryItemDto[]>
   */
  async findAllWords(userToken: string): Promise<DictionaryItemDto[]> {
    const user = await this.userService.getUserByToken(userToken)
    return await this.dictionaryRepository.findAll({
      include: {
        attributes: [],
        where: {
          id: user.id,
        },
        required: true,
        association: 'users',
      },
    })
  }

  /**
   * Добавление слова в словарь
   * @param word Omit<DictionaryItemDto, 'localId'>
   * @param request Request
   * @returns
   */
  async addWordToDictionary(word: Omit<DictionaryItemDto, 'localId'>, request: Request): Promise<number | void> {
    try {
      const leoResponse = await addLeoDictionary(word, request)
      const user = await this.userService.getUserByToken(getLocalToken(request))

      if (!leoResponse) return getBadRequest('Не получилось добавить слово в сервис')

      return await this.checkAndAddWord(word, user)
    } catch (e) {
      getBadRequest('Не получилось добавить слово')
    }
  }

  /**
   * Удаление слова
   * @param word RemoveDictionaryRequestDt
   * @param request Request
   * @returns
   */
  async removeWordFromDictionary(word: RemoveDictionaryRequestDto, request: Request) {
    try {
      const token = getLeoToken(request)
      const user = await this.userService.getUserByToken(getLocalToken(request))

      const { status } = await getLeoRequest(token, SET_LINGUALEO_DICTIONARY_API, getRemoveData(word))
      if (status === RESPONSE_STATUSES.OK) {
        return this.dictionaryUserRepository.destroy({
          where: {
            dictionaryId: word.localId,
            userId: user.id,
          },
        })
      }
    } catch (e) {
      getBadRequest('Не удалось удалить слово')
    }
  }

  /**
   * Синхронизация с словарем пользователя в Лео
   * @param request Request
   * @returns
   */
  async synchronizeDictionary(request: Request) {
    const words = []
    const user = await this.userService.getUserByToken(getLocalToken(request))
    const leoDictionary = await getLeoDictionary(request)
    leoDictionary.data.forEach((set) => {
      set.words.forEach((word) => {
        this.checkAndAddWord(word, user)
        words.push(word)
      })
    })

    return words
  }

  /**
   * Проверка на существование слова в базу и добавление его
   * @param word Omit<DictionaryItemDto, 'localId'>
   * @param user IUser
   */
  private async checkAndAddWord(word: Omit<DictionaryItemDto, 'localId'>, user: IUser): Promise<number> {
    try {
      const [dictionary, _] = await this.dictionaryRepository.findOrCreate({
        raw: true,
        where: { id: word.id },
        defaults: word,
      })

      const dictionaryId = dictionary.getDataValue ? dictionary.getDataValue('localId') : dictionary['localId']

      const deaultDictionaryUser = {
        dictionaryId,
        userId: user.id,
      }

      await this.dictionaryUserRepository.findOrCreate({
        raw: true,
        where: { ...deaultDictionaryUser },
        defaults: deaultDictionaryUser,
      })

      return dictionaryId
    } catch (err: any) {
      getBadRequest('Не удалось добавить слово')
    }
  }
}
