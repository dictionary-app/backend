import { Request } from 'express'
import { getLeoToken } from 'src/helpers'
import { DictionaryItemDto } from './dto/dictionary-item.dto'
import { IDictionaryItem, ISearchLeoResponse } from './types'

import { GET_LINGUALEO_DICTIONARY_API, LEO_TOKEN, SET_LINGUALEO_DICTIONARY_API } from 'src/configs'
import { HttpService } from '@nestjs/common'
import { RemoveDictionaryRequestDto } from './dto/remove-dictionary-item.dto'

const API_VERSION_LEO = '1.0.1'

export function getLeoRequest(token: string, api: string, data) {
  const options = {
    headers: {
      Cookie: `${LEO_TOKEN}=${token}`,
    },
  }

  const httpService = new HttpService()

  return httpService
    .post(api, data, options)
    .toPromise()
    .then((res) => res.data)
}

export async function addLeoDictionary(word: Omit<DictionaryItemDto, 'localId'>, request: Request) {
  const leoToken = getLeoToken(request)
  const data = {
    apiVersion: API_VERSION_LEO,
    data: [
      {
        action: 'add',
        wordIds: [word.id],
        valueList: { wordValue: word.wordValue, translation: { id: word.id, tr: word.translateValue } },
      },
    ],
    op: 'actionWithWords {action: add}',
    userData: { nativeLanguage: 'lang_id_src' },
  }

  return this.getLeoRequest(leoToken, SET_LINGUALEO_DICTIONARY_API, data)
}

export function getLeoDictionary(request: Request, data = {}) {
  data = {
    apiVersion: API_VERSION_LEO,
    attrList: this.getAttrsList(),
    perPage: 5000,
    ...data,
  }

  return this.getLeoRequest(getLeoToken(request), GET_LINGUALEO_DICTIONARY_API, data)
}

export function getAttrsList() {
  return {
    association: 'as',
    translateValue: 'trc',
    created: 'cd',
    id: 'id',
    learningStatus: 'ls',
    listWordSets: 'listWordSets',
    origin: 'wo',
    picture: 'pic',
    progress: 'pi',
    pronunciation: 'pron',
    relatedWords: 'rw',
    speechPartId: 'pid',
    trainings: 'trainings',
    transcription: 'scr',
    translations: 'trs',
    wordLemmaId: 'lid',
    wordLemmaValue: 'lwd',
    wordSets: 'ws',
    wordType: 'wt',
    wordValue: 'wd',
    twordId: 'twordId',
  }
}

export function formatRequestSearchLeo(data: ISearchLeoResponse): Omit<IDictionaryItem, 'localId'>[] {
  const words: Omit<IDictionaryItem, 'localId'>[] = []
  data.translate.forEach((w) => {
    words.push({
      id: +data.id,
      twordId: +w.id,
      wordValue: data.word_value,
      transcription: data.transcription,
      translateValue: w.translate_value,
      picture: w.pic_url,
      useCase: null,
      pronunciation: data.sound_url,
    })
  })
  return words
}

export function getRemoveData(word: RemoveDictionaryRequestDto) {
  return {
    apiVersion: '1.0.1',
    op: 'groupActionWithWords {action: delete}',
    data: [
      {
        action: 'delete',
        mode: 'delete',
        wordIds: [word.id],
        wordSetId: 1,
        valueList: { globalSetId: 1 },
        chunk: null,
        dateGroups: null,
      },
    ],
    userData: { nativeLanguage: 'lang_id_src' },
  }
}
