import { IDictionaryItemBase } from './../types/index'
import { ApiProperty } from '@nestjs/swagger'

export class UpdateDictionaryDto implements IDictionaryItemBase {
  @ApiProperty({ example: '21312312312', description: 'Идентификатор слова' })
  id: number

  @ApiProperty({ example: 123123123213, description: 'Идентификатор перевода слова' })
  twordId: number

  @ApiProperty({ example: 'lingua', description: 'Английское значение слова' })
  wordValue: string

  @ApiProperty({ example: 'lˈɪŋgwə', description: 'Транскрипция' })
  transcription: string

  @ApiProperty({ example: 'язык,языковой', description: 'Перевод' })
  translateValue: string

  @ApiProperty({ example: 'https://google.com/image.png', description: 'Url картинки' })
  picture: string

  @ApiProperty({ example: 'We will not permit this, Alfonso and I!', description: 'Пример использования' })
  useCase: string

  @ApiProperty({ example: 'https:///bGluZ3Vhc7c9e6d1.mp3', description: 'Url озвучки' })
  pronunciation: string
}
