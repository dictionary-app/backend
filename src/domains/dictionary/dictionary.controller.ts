import { HttpService } from '@nestjs/axios'
import { Controller, Get, Post, Body, Param, Delete, Req, UseGuards } from '@nestjs/common'
import { Request } from 'express'
import { ApiCookieAuth, ApiTags } from '@nestjs/swagger'
import { DictionaryService } from './dictionary.service'
import { getLeoToken, getLocalToken } from 'src/helpers'
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard'
import { DictionaryItemDto } from './dto/dictionary-item.dto'
import { RemoveDictionaryRequestDto } from './dto/remove-dictionary-item.dto'

@Controller('dictionary')
@ApiTags('Dictionary')
export class DictionaryController {
  constructor(private readonly dictionaryService: DictionaryService, private readonly httpService: HttpService) {}

  @Get('search/:text')
  async search(@Param('text') text: string, @Req() request: Request) {
    return this.dictionaryService.search(text, getLeoToken(request))
  }

  @Get('origin/search/:text')
  async originSearch(@Param('text') text: string, @Req() request: Request) {
    return this.dictionaryService.originSearch(text, getLeoToken(request))
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Get()
  findAllWords(@Req() request: Request): Promise<DictionaryItemDto[]> {
    return this.dictionaryService.findAllWords(getLocalToken(request))
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Get('synchronize')
  synchronizeDictionary(@Req() request: Request) {
    return this.dictionaryService.synchronizeDictionary(request)
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Post()
  addWordToDictionary(@Body() item: DictionaryItemDto, @Req() request: Request): Promise<number | void> {
    return this.dictionaryService.addWordToDictionary(item, request)
  }

  @UseGuards(JwtAuthGuard)
  @ApiCookieAuth()
  @Delete()
  removeWordFromDictionary(@Body() item: RemoveDictionaryRequestDto, @Req() request: Request) {
    return this.dictionaryService.removeWordFromDictionary(item, request)
  }
}
