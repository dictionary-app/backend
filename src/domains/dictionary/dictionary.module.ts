import { UsersService } from './../users/users.service'
import { UserModel } from './../users/user.model'
import { DictionaryModel } from './dictionary.model'
import { Module } from '@nestjs/common'
import { DictionaryService } from './dictionary.service'
import { DictionaryController } from './dictionary.controller'
import { SequelizeModule } from '@nestjs/sequelize'
import { DictionaryUserModel } from './dictionary-users.model'
import { HttpModule } from '@nestjs/axios'
import { GroupDictionaryModel } from '../groups/group-dictionary.model'

@Module({
  controllers: [DictionaryController],
  imports: [
    HttpModule,
    SequelizeModule.forFeature([DictionaryModel, UserModel, DictionaryUserModel, GroupDictionaryModel]),
  ],
  providers: [DictionaryService, UsersService],
})
export class DictionaryModule {}
