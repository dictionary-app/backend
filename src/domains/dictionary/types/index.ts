export const RESPONSE_STATUSES = {
  OK: 'ok',
  ERROR: 'error',
}

export interface IDictionaryItemBase {
  id: number
  twordId: number
  wordValue: string
  transcription?: string | null
  translateValue: string
  picture: string | null
  useCase: string | null
  pronunciation?: string | null
}

export interface IDictionaryItem extends IDictionaryItemBase {
  localId: number
}

export interface IDictionaryUserItem {
  id: number
  dictionaryId: number
  userId: number
}

interface ILeoTranslateItem {
  ctx: string
  ctx_tr: string
  id: number
  pic_url: string
  pos: string
  pr: string
  tr: string
  translate_value: string
  twordId: string
  value: string
  votes: number
  vt: number
}

export interface ISearchLeoResponse {
  apiVersion: string
  destLang: number
  directionEnglish: boolean
  id: number | string | null
  invertTranslateDirection: boolean
  is_user: number
  sound_url: string
  srcLang: number
  status: string
  translate: ILeoTranslateItem[]
  transcription: string
  translation: string
  wordLemmaId: number
  wordLemmaValue: string
  word_id: number | string | null
  word_top: number
  word_type: number
  word_value: string
}
