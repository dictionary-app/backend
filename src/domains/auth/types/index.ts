export interface ITokens {
  leoToken: string
  userToken: string
}
