import { Body, Controller, Get, Post, Res, UseGuards } from '@nestjs/common'
import { ApiBody, ApiOperation, ApiResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger'
import { Response } from 'express'
import { AuthService } from './auth.service'
import { LoginRequestDto } from './dto/login-request.dto'
import { LoginResponseDto } from './dto/login-response.dto'
import { LocalAuthGuard } from './guards/local-auth.guard'

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/login')
  @UseGuards(LocalAuthGuard)
  @ApiOperation({ summary: 'Аутентификация пользователя' })
  @ApiResponse({ status: 200, type: LoginResponseDto })
  @ApiBody({ type: LoginRequestDto })
  @ApiUnauthorizedResponse({ description: 'Неверные входные данные' })
  public async login(
    @Body() user: LoginRequestDto,
    @Res({ passthrough: true }) response: Response
  ): Promise<LoginResponseDto> {
    return this.authService.login({ user, response })
  }

  @Get('logout')
  async logout(@Res({ passthrough: true }) response: ResponseType): Promise<void> {
    return this.authService.logout(response)
  }
}
