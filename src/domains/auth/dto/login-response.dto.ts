import { UserEntity } from 'src/domains/users/entity/user.entity'

export class LoginResponseDto {
  readonly user: UserEntity
}
