import { HttpException, HttpService, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { UsersService } from 'src/domains/users/users.service'
import * as bcrypt from 'bcryptjs'
import { LoginRequestDto } from './dto/login-request.dto'
import { LoginResponseDto } from './dto/login-response.dto'
import { UserEntity } from '../users/entity/user.entity'
import { LEO_TOKEN, LOCAL_TOKEN, LOGIN_LEO_API } from 'src/configs'

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
    private readonly httpService: HttpService
  ) {}

  async login({ user: userEntity, response }): Promise<LoginResponseDto> {
    const { accessToken } = await this.loginLeo(userEntity.email, userEntity.password)

    const user = await this.userService.getUserByEmailWithoutPassword(userEntity.email)

    if (user) {
      response.cookie(LOCAL_TOKEN, this.generateToken(user), {
        httpOnly: true,
        secure: false, //--> SET TO TRUE ON PRODUCTION
      })

      response.cookie(LEO_TOKEN, accessToken, {
        httpOnly: true,
        secure: false, //--> SET TO TRUE ON PRODUCTION
      })
    } else {
      const registerResponse = await this.register({ user: userEntity, response })
      response.cookie(LEO_TOKEN, accessToken, {
        httpOnly: true,
        secure: false, //--> SET TO TRUE ON PRODUCTION
      })

      return registerResponse
    }
    return { user }
  }

  async register({ user: userEntity, response }): Promise<LoginResponseDto> {
    const defaultUser = {
      name: '',
    }

    const isUserFound = await this.userService.checkUniqueUser(userEntity.email)
    if (isUserFound) {
      throw new HttpException('Пользователь с таким логином или e-mail уже существует', HttpStatus.BAD_REQUEST)
    }

    const salt = bcrypt.genSaltSync(10)
    const hashPassword = await bcrypt.hashSync(userEntity.password, salt)
    const user = await this.userService.createUser({
      ...defaultUser,
      ...userEntity,
      password: hashPassword,
    })

    response.cookie(LOCAL_TOKEN, this.generateToken(user), {
      httpOnly: true,
      secure: false, //--> SET TO TRUE ON PRODUCTION
    })

    return { user }
  }

  async loginLeo(email: string, password: string) {
    const data = { type: 'mixed', credentials: { email, password } }

    return this.httpService
      .post(LOGIN_LEO_API, data)
      .toPromise()
      .then((res) => res.data)
  }

  async logout(response): Promise<void> {
    response.cookie(LOCAL_TOKEN, '', { expires: new Date() })
    response.cookie(LEO_TOKEN, '', { expires: new Date() })
    return
  }

  private generateToken(user) {
    const payload = { email: user.email }
    return this.jwtService.sign(payload)
  }

  async validateUser(userDto: LoginRequestDto): Promise<UserEntity> {
    const user = await this.userService.getUserByEmail(userDto.email)
    if (!user) throw new UnauthorizedException({ message: 'Email не найден' })

    const passwordEquals = await bcrypt.compare(userDto.password, user.password)

    if (passwordEquals) return user
    throw new UnauthorizedException({
      message: 'Неверный пароль',
    })
  }
}
