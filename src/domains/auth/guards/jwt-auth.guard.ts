import { LOCAL_TOKEN } from './../../../configs/tokens'
import { Injectable } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'

@Injectable()
export class JwtAuthGuard extends AuthGuard(LOCAL_TOKEN) {}
